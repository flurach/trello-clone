const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const app = express()

// plugins
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cookieParser('legen, wait for it... dary! legendary!'))

// api
app.use('/auth', require('./auth/routes'))
app.use('/projects', require('./projects/routes'))

// react app
app.use(express.static('build'))
app.get('*', (req, res) => res.redirect('/'))

// listen
const PORT = process.env.PORT || 8080
app.listen(PORT, () => console.log(`Listning on port ${PORT}`))