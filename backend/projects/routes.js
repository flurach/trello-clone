const router = require('express').Router()
const auth = require('../auth/middlewares')
const controllers = require('./controllers')

router.post('/create', auth.check(), controllers.create)
router.post('/read', auth.check(), controllers.read)
router.post('/delete', auth.check(), controllers.remove)

router.post('/add-stage', auth.check(), controllers.add_stage)
router.post('/delete-stage', auth.check(), controllers.remove_stage)

router.post('/add-card', auth.check(), controllers.add_card)
router.post('/move-card', auth.check(), controllers.move_card)
router.post('/delete-card', auth.check(), controllers.remove_card)

module.exports = router