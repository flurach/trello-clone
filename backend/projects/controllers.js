const { projects } = require('./models')
const validator = require('validator')


async function create(req, res) {

	// validate fields
	const name = req.body.name || ''
	const description = req.body.description || ''
	if (!name)
		return res.send({ error: 'Fields are invalid' })

	// insert project
	const owner = req.user._id
	projects.asyncInsert({ owner, name, description, stages: {} })
	res.send({})

}


async function read(req, res) {

	// return list of projects
	const owner = req.user._id
	res.send(await projects.asyncFind({ owner }, { owner: 0 }))

}


async function remove(req, res) {

	// validate fields
	const { project_id } = req.body
	if (!project_id)
		return res.send({ error: 'Fields are invalid' })

	// check if project is owned by user
	const owner = req.user._id
	const project = await projects.asyncFindOne({ _id: project_id, owner })
	if (!project)
		return res.send({ error: 'You do not own this project' })

	// remove project
	projects.asyncRemove({ _id: project_id })
	res.send({})

}


async function add_stage(req, res) {

	// validate fields
	const { project_id, stage } = req.body
	if (!project_id || !stage)
		return res.send({ error: 'Fields are invalid' })

	// check if project exists
	const owner = req.user._id
	const project = await projects.asyncFindOne({ _id: project_id, owner  })
	if (!project)
		return res.send({ error: 'Project does not exist' })

	// add stage
	project.stages[stage] = []
	projects.asyncUpdate({ _id: project_id }, {
		$set: { stages: project.stages }
	})
	res.send({})

}


async function remove_stage(req, res) {

	// validate fields
	const { project_id, stage } = req.body
	if (!project_id || !stage)
		return res.send({ error: 'Fields are invalid' })

	// check if project exists
	const project = await projects.asyncFindOne({ _id: project_id })
	if (!project)
		return res.send({ error: 'Project does not exist' })

	// check stage
	if (project.stages.hasOwnProperty(stage) === false)
		return res.send({ error: 'Stage id is invalid' })

	// remove stage
	delete project.stages[stage]
	projects.asyncUpdate({ _id: project_id }, {
		$set: { stages: project.stages }
	})
	res.send({})

}


async function add_card(req, res) {

	// validate fields
	const { project_id, stage, card } = req.body
	if (!project_id || !stage || !card)
		return res.send({ error: 'Fields are invalid' })

	// check if project exists
	const owner = req.user._id
	const project = await projects.asyncFindOne({ _id: project_id, owner })
	if (!project)
		return res.send({ error: 'You do not own this project' })

	// check if stage exists
	if (!project.stages.hasOwnProperty(stage))
		return res.send({ error: 'Stage id is invalid' })

	// add card
	project.stages[stage].push(card)
	projects.asyncUpdate({ _id: project_id }, {
		$set: { stages: project.stages }
	})
	res.send({})

}


async function move_card(req, res) {

	// validate fields
	const { project_id, stage, card_id, new_stage } = req.body
	if (!project_id || !stage || !validator.isNumeric(card_id))
		return res.send({ error: 'Fields are invalid' })

	// check if project exists
	const owner = req.user._id
	const project = await projects.asyncFindOne({ _id: project_id, owner })
	if (!project)
		return res.send({ error: 'You do not own this project' })

	// check if stages exists
	if (!project.stages.hasOwnProperty(stage))
		return res.send({ error: 'Stage id is invalid' })
	else if (!project.stages.hasOwnProperty(new_stage))
		return res.send({ error: 'New stage id is invalid' })

	// check if card exists
	const card_index = parseInt(card_id)
	if (project.stages[stage].length < card_index)
		return res.send({ error: 'Card id is invalid' })

	// move card
	const card_item = project.stages[stage].splice(card_index, 1)[0]
	project.stages[new_stage].unshift(card_item)

	// update
	projects.asyncUpdate({ _id: project_id }, {
		$set: { stages: project.stages }
	})
	res.send({})

}


async function remove_card(req, res) {

	// validate fields
	const { project_id, stage, card_id } = req.body
	if (!project_id || !stage || !validator.isNumeric(card_id))
		return res.send({ error: 'Fields are invalid' })

	// check if project exists
	const owner = req.user._id
	const project = await projects.asyncFindOne({ _id: project_id, owner })
	if (!project)
		return res.send({ error: 'You do not own this project' })

	// check if stage exists
	if (!project.stages.hasOwnProperty(stage))
		return res.send({ error: 'Stage id is invalid' })

	// check if card exists
	const card_index = parseInt(card_id)
	if (project.stages[stage].length <= card_index)
		return res.send({ error: 'Card id is invalid' })

	// remove card
	project.stages[stage].splice(card_index, 1)
	projects.asyncUpdate({ _id: project_id }, {
		$set: { stages: project.stages }
	})
	res.send({})

}


module.exports = {
	create, read, remove,
	add_stage, remove_stage,
	add_card, move_card, remove_card
}