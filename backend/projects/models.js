const { AsyncNedb } = require('nedb-async')

const projects = new AsyncNedb({
	filename: 'backend/.data/projects.db',
	autoload: true
})

module.exports = { projects }