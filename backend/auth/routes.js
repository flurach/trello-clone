const router = require('express').Router()
const controllers = require('./controllers')

router.post('/register', controllers.register)
router.post('/login', controllers.login)
router.post('/logout', controllers.logout)
router.post('/check', controllers.check)

module.exports = router