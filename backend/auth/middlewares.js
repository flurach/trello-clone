const { users, tokens } = require('./models')


function check(options = {}) {
	const redirectUrl = options.redirectUrl || '/'

	return async function(req, res, next) {
		const { AUTH_TOKEN } = req.signedCookies
		if (AUTH_TOKEN === undefined)
			return res.redirect(redirectUrl)

		// check if token is valid		
		const token = await tokens.asyncFindOne({ _id: AUTH_TOKEN })
		if (!token) {
			res.cookie('AUTH_TOKEN', '', { expires: new Date(Date.now()) })
			return res.redirect(redirectUrl)
		}

		// add user to request
		const user = await users.asyncFindOne(
			{ _id: token.user }, { password: 0 }
		)
		req.user = user
		next()
	}
}


module.exports = { check }