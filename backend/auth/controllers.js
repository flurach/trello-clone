const { isEmail } = require('validator')
const { users, tokens } = require('./models')
const bcrypt = require('bcrypt')


async function register(req, res) {

	// validate fields
	const email = req.body.email || ''
	const password = req.body.password || ''
	if (!isEmail(email) || password.length < 8)
		return res.send({ error: 'Fields are invalid' })

	// check if email exists
	const user = await users.asyncFindOne({ email })
	if (user)
		return res.send({ error: 'User with that email exists '})

	// insert user
	const hashed = await bcrypt.hash(password, 10)
	users.asyncInsert({ email, password: hashed })
	res.send({})

}


async function login(req, res) {

	// validate fields
	const email = req.body.email || ''
	const password = req.body.password || ''
	if (!isEmail(email) || password.length < 8)
		return res.send({ error: 'Fields are invalid' })

	// get user
	const user = await users.asyncFindOne({ email })
	if (!user)
		return res.send({ error: 'User does not exist' })

	// check password
	const passwords_match = await bcrypt.compare(password, user.password)
	if (!passwords_match)
		return res.send({ error: 'Password is invalid' })

	// log user in
	const token = await tokens.asyncInsert({ user: user._id })
	res.cookie('AUTH_TOKEN', token._id, {
		httpOnly: true, signed: true, sameSite: true
	})

	res.send({})

}


async function logout(req, res) {
	const all_devices = req.body.all_devices || false

	// check token
	const { AUTH_TOKEN } = req.signedCookies
	if (!AUTH_TOKEN)
		return res.send({})

	// get token
	const token = await tokens.asyncFindOne({ _id: AUTH_TOKEN })
	if (!token)
		return res.send({})

	// remove token(s) from models
	if (all_devices)
		tokens.asyncRemove({ user: token.user }, { multi: true })
	else
		tokens.asyncRemove({ _id: AUTH_TOKEN })

	res.send({})
}


async function check(req, res) {
	const { AUTH_TOKEN } = req.signedCookies
	if (AUTH_TOKEN === undefined)
		return res.send({})

	// check if token is valid		
	const token = await tokens.asyncFindOne({ _id: AUTH_TOKEN })
	if (!token) {
		res.cookie('AUTH_TOKEN', '', { expires: new Date(Date.now()) })
		return res.send({})
	}

	// get user
	const user = await users.asyncFindOne({ _id: token.user }, { password: 0 })
	res.send(user)
}


module.exports = { register, login, logout, check }