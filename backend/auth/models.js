const { AsyncNedb } = require('nedb-async')

const users = new AsyncNedb({
	filename: 'backend/.data/users.db',
	autoload: true
})

const tokens = new AsyncNedb({
	filename: 'backend/.data/tokens.db',
	autoload: true
})

module.exports = { users, tokens }