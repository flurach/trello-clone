import * as React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import 'skeleton-css/css/normalize.css'
import 'skeleton-css/css/skeleton.css'
import './App.css'

import Home from './pages/home'
import Welcome from './pages/welcome'
import Register from './pages/register'
import RegisterSuccess from './pages/register-success'
import Login from './pages/login'
import Logout from './pages/logout'


const App = () =>
	<BrowserRouter>
		<Switch>
			<Route exact path="/" component={Home}/>
			<Route path="/welcome" component={Welcome}/>
			<Route path="/register" component={Register}/>
			<Route path="/register-success" component={RegisterSuccess}/>
			<Route path="/login" component={Login}/>
			<Route path="/logout" component={Logout}/>
		</Switch>
	</BrowserRouter>


export default App