import Axios from 'axios'


async function check() {
	const { data } = await Axios.post('/auth/check')

	return Object.keys(data).length
		? data
		: false
}

async function register(email, password) {
	const { data } = await Axios.post('/auth/register', { email, password })
	return data
}

async function login(email, password) {
	const { data } = await Axios.post('/auth/login', { email, password })
	return data
}

async function logout(all_devices = false) {
	await Axios.post('/auth/logout', { all_devices })
}


export { check, register, login, logout }