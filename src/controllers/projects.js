import Axios from 'axios'


async function list() {
	const { data } = await Axios.post('/projects/read')
	return data
}

async function create(name, description) {
	const { data } = await Axios.post('/projects/create', { name, description })
	return data
}

async function remove(project_id) {
	const { data } = await Axios.post('/projects/delete', { project_id })
	return data
}


export { list, create, remove }