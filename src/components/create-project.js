import * as React from 'react'
import * as Projects from '../controllers/projects'


function CreateActual({ setDisplay }) {
	const [name, setName] = React.useState('')
	const [description, setDescription] = React.useState('')

	async function createProject() {
		const data = await Projects.create(name, description)
		if (data.error)
			return alert(data.error)

		setDisplay('created')
	}

	return <div className="create-template" style={styles.form.container}>
		<input
			onChange={e => setName(e.target.value)}
			placeholder="Project Name..."/>
		<textarea
			onChange={e => setDescription(e.target.value)}
			placeholder="Description..."
			style={{ resize: 'vertical', transition: '0s' }}></textarea>

		<div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
		<button className="button-primary" onClick={createProject}>Create</button>
		<button onClick={() => setDisplay(false)}>Cancel</button>
		</div>
	</div>
}

function CreateTemplate({ setDisplay }) {
	return <div
		className="create-template"
		style={styles.temp.container}
		onClick={() => setDisplay(true)}>
			<p style={styles.temp.text}>Add Project</p>
	</div>
}

export default ({ setUpdateProjects }) => {
	const [display, setDisplay] = React.useState(false)

	React.useEffect(() => {
		if (display === 'created') {
			setUpdateProjects(true)
			setDisplay(false)
		}
	}, [display, setUpdateProjects])

	return display
		? <CreateActual setDisplay={setDisplay}/>
		: <CreateTemplate setDisplay={setDisplay}/>
} 

const styles = {
	temp: {
		container: {
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			height: '200px',
			border: '2px dashed white',
			borderRadius: '10px',
			background: 'rgba(255, 255, 255, .2)',
		},
		text: {
			marginBottom: '0px',
		},
	},
	form: {
		container: {
			display: 'grid',
			gridGap: '10px',
			padding: '10px',
			border: '2px dashed white',
			borderRadius: '10px',
			background: 'rgba(255, 255, 255, .2)',
		},
	},
}