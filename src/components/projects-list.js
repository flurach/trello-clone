import * as React from 'react'
import * as Projects from '../controllers/projects'

import CreateProject from '../components/create-project'
import ProjectIcon from '../components/project-icon'


export default () => {
	// state
	const [projects, setProjects] = React.useState([])
	const [updateProjects, setUpdateProjects] = React.useState(false)

	// get projects
	React.useEffect(() => {Projects.list().then(data => {
		setProjects(data)
		if (updateProjects)
			setUpdateProjects(false)
	})}, [updateProjects])

	// view
	return <div style={styles.container}>
		<h3>Your Projects</h3>

		<div style={styles.projects}>
			{projects.map((data, i) =>
				<ProjectIcon key={i} data={data} setUpdateProjects={setUpdateProjects}/>
			)}
			<CreateProject setUpdateProjects={setUpdateProjects}/>
		</div>
	</div>
}

const styles = {
	container: {
		display: 'flex',
		flexDirection: 'column',
		margin: '4%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	projects: {
		display: 'grid',
		gridTemplateColumns: 'repeat(3, 33%)',
		gridGap: '10px',
		width: '100%',
	},
}