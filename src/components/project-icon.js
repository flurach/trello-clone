import * as React from 'react'
import * as Projects from '../controllers/projects'


export default ({ data, setUpdateProjects }) => {
	const { _id, name, description } = data

	async function remove() {
		if (window.confirm("Do you want to delete the project?") === false)
			return

		const data = await Projects.remove(_id)
		if (data.error)
			return alert(data.error)

		setUpdateProjects(true)
	}

	return <div style={styles.container}>
		<h4>{ name }</h4>
		<p>{ description }</p>
		<div style={styles.buttons}>
			<button className="button-primary">Display</button>
			<button onClick={remove}>Delete</button>
		</div>
	</div>
}

const styles = {
	container: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		boxSizing: 'border-box',
		padding: '10px',
		height: '204px',
		background: 'white',
		borderRadius: '10px',
	},
	buttons: {
		display: 'grid',
		gridTemplateColumns: 'repeat(2, 50%)',
		gridGap: '5px',
	},
}