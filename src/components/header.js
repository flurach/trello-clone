import * as React from 'react'
import { Link } from 'react-router-dom'


export default ({ email }) =>
	<header style={styles.container}>

		<div>
			<h5 style={styles.title}><strong>Trello Clone</strong></h5>
			<p style={styles.title}>{ email }</p>
		</div>
		<div>
			<Link to="/logout">
				<button style={styles.title} className="button-primary">
					Log out
				</button>
			</Link>
		</div>

	</header>

const styles = {
	container: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '5px 20px',
		width: 'calc(100% - 40px)',
		background: 'rgba(255, 255, 255, .2)',
	},
	title: {
		marginBottom: '0px',
		color: 'white',
	},
}