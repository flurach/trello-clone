import * as React from 'react'
import { useHistory } from 'react-router-dom'
import * as Auth from '../controllers/auth'


export default () => {
	const history = useHistory()

	// check if user is logged in
	Auth.check().then(data => {
		if (data !== false)
			history.push('/')
	})

	return <div style={styles.center}>
		<h1>Welcome to Trello Clone</h1>
		<p>You seem to be not logged in</p>

		<div style={styles.buttons}>
			<button
				onClick={() => history.push('/login')}
				style={{marginBottom: '0px'}}>
				Log in Now</button>
			or
			<button
				onClick={() => history.push('/register')}
				className="button-primary"
				style={{marginBottom: '0px'}}>
				Create an Account</button>
		</div>
	</div>
}

const styles = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
	buttons: {
		display: 'grid',
		gridTemplateColumns: 'repeat(3, auto)',
		gridGap: '10px',
		alignItems: 'center',
	},
}