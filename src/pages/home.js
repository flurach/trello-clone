import * as React from 'react'
import { useHistory } from 'react-router-dom'
import * as Auth from '../controllers/auth'

import Header from '../components/header'
import ProjectsList from '../components/projects-list'


export default () => {
	const history = useHistory()

	// state
	const [userData, setUserData] = React.useState({})

	// check if user is logged in
	React.useEffect(() =>	{Auth.check().then(data => {
		if (data === false)
			history.push('/welcome')
		setUserData(data)
	})}, [history])

	// view
	return <div style={styles.container}>
		<Header email={userData.email}/>
		<ProjectsList/>
	</div>
}

const styles = {
	container: {
		width: '100vw',
		height: '100vh',
		background: 'url("images/wallpaper.jpg") no-repeat center/cover',
	}
}