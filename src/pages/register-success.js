import * as React from 'react'
import { Link, useHistory } from 'react-router-dom'
import * as Auth from '../controllers/auth'


export default () => {
	const history = useHistory()

	// check if user is logged in
	Auth.check().then(data => {
		if (data !== false || !localStorage.register_success)
			history.push('/')

		localStorage.removeItem('register_success')
	})

	return <div style={styles.center}>
		<h1 style={{ color: 'limegreen' }}>Well Done!</h1>
		<p>We have successfully created your account</p>
		<p>
			You can now proceed and
			<Link style={{ marginLeft: '5px' }} to="/login">
				log in
			</Link>
		</p>
	</div>
}

const styles = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
}