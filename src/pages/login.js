import * as React from 'react'
import { Link, useHistory } from 'react-router-dom'
import * as Auth from '../controllers/auth'
import { isEmail } from 'validator'


export default () => {
	const history = useHistory()

	// check if user is logged in
	Auth.check().then(data => {
		if (data !== false)
			history.push('/')
	})

	// state
	const [email, setEmail] = React.useState('')
	const [password, setPassword] = React.useState('')
	const [error, setError] = React.useState('')

	// methods
	async function login() {

		// check field
		if (!isEmail(email))
			return setError('Email is invalid')

		// try to login
		const data = await Auth.login(email, password)
		if (data.error)
			return setError(data.error)

		history.push('/')

	}

	// view
	return <div style={styles.center}>
		<h1 style={styles.title}>Trello Clone</h1>
		<p>Log In</p>

		<input
			onChange={e => setEmail(e.target.value)}
			type="email"
			placeholder="Email..."/>
		<input
			onChange={e => setPassword(e.target.value)}
			type="password"
			placeholder="Password..."/>

		<button onClick={login} className="button-primary">
			Log in
		</button>

		<p
			style={{ ...styles.error, display: error ? '' : 'none' }}>
			{ error }
		</p>

		<Link to="/register">Not have an account?</Link>
	</div>
}

const styles = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
	title: {
		marginBottom: '0px'
	},
	error: {
		padding: '10px',
		color: 'tomato',
	}
}