import * as React from 'react'
import { Link, useHistory } from 'react-router-dom'
import * as Auth from '../controllers/auth'
import { isEmail } from 'validator'


export default () => {
	const history = useHistory()

	// check if user is logged in
	Auth.check().then(data => {
		if (data !== false)
			history.push('/')
	})

	// state
	const [email, setEmail] = React.useState('')
	const [password, setPassword] = React.useState('')
	const [passwordAgain, setPasswordAgain] = React.useState('')
	const [error, setError] = React.useState('')

	// methods
	async function register() {

		// check fields
		if (!isEmail(email))
			return setError('Email is invalid')
		else if (password !== passwordAgain)
			return setError('Passwords do not match')
		else if (password.length < 8)
			return setError('Password must be at least 8 characters')

		// try to register user
		const data = await Auth.register(email, password)
		if (data.error)
			return setError(data.error)

		localStorage.register_success = true
		history.push('/register-success')

	}

	// view
	return <div style={styles.center}>
		<h1 style={styles.title}>Trello Clone</h1>
		<p>Sign Up</p>

		<input
			onChange={e => setEmail(e.target.value)}
			type="email"
			placeholder="Email..."/>
		<input
			onChange={e => setPassword(e.target.value)}
			type="password"
			placeholder="Password..."/>
		<input
			onChange={e => setPasswordAgain(e.target.value)}
			type="password"
			placeholder="Password (Again)..."/>

		<button onClick={register} className="button-primary">
			Sign Up
		</button>

		<p
			style={{ ...styles.error, display: error ? '' : 'none' }}>
			{ error }
		</p>

		<Link to="/login">Have an account?</Link>
	</div>
}

const styles = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
	title: {
		marginBottom: '0px'
	},
	error: {
		padding: '10px',
		color: 'tomato',
	}
}