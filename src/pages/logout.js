import * as React from 'react'
import { useHistory, Link } from 'react-router-dom'
import * as Auth from '../controllers/auth'


export default () => {
	const history = useHistory()

	// check if user is logged in
	Auth.check().then(data => {
		if (data === false)
			history.push('/')
	})

	// methods
	function logout(all = false) {
		Auth.logout(all).then(() => history.push('/welcome'))
	}

	return <div style={styles.center}>
		<Link to="/">Go back</Link>
		<h1 style={{ paddingBottom: '30px' }}>Are you sure?</h1>

		<button onClick={logout} className="button-primary">
			Log out now
		</button>

		<p onClick={() => logout(true)} style={styles.link}>
			Or log out from all devices
		</p>
	</div>
}

const styles = {
	center: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		height: '100vh',
	},
	link: {
		color: 'deepskyblue',
		cursor: 'pointer',
		textDecoration: 'underline',
	},
}