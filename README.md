# Trello Clone
Made with React & Express


## How to get going?
First, have:

* yarn
* node

And then do `yarn` to install dependencies.
Then build react with `yarn build` and start express with `yarn start`


## Development

You will need 2 terminals.
Go to the first one and start react with `yarn dev`.
Next, go to the other one and start express with `yarn server`

That's it. Visit http://localhost:3000/
